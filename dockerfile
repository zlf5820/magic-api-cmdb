FROM openjdk:8
ADD target/magic-api-cmdb-0.0.1-SNAPSHOT.jar /app.jar
EXPOSE 9999
ENTRYPOINT java -jar app.jar
#ENTRYPOINT ["Bash","-DBash.security.egd=file:/dev/./urandom","-jar","/app.jar"]
#ENTRYPOINT ["Bash","-DBash.security.egd=file:/dev/./urandom","-jar","/app.jar","--spring.profiles.active=prd"]
#CMD ["java","-jar","app.jar"]