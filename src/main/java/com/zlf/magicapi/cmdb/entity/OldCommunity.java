package com.zlf.magicapi.cmdb.entity;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;

@Data
public class OldCommunity {
    @JsonProperty("data")
    private Datas datas;
    private String interfaceCode;
    private String sessionId;

    @Data
    public static class Datas {
        private String alarmChannelCode;
        private String alarmChannelName;
        private String alarmCode;
        private String alarmPic1;
        private int alarmStat;
        private String alarmTime;
        private int alarmType;
        private String deviceCode;
        private String deviceName;
        private String message;
    }
}
