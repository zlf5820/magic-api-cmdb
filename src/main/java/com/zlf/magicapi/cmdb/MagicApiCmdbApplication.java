package com.zlf.magicapi.cmdb;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class MagicApiCmdbApplication {

    public static void main(String[] args) {
        SpringApplication.run(MagicApiCmdbApplication.class, args);
    }

}
